import os


class TestProcessEndpoint:
    ENDPOINT = '/diagnostics/process'

    async def test_get(self, aiohttp_client, app):
        client = await aiohttp_client(app)

        response = await client.get(self.ENDPOINT)
        response_content = await response.json()

        assert response.status == 200
        assert response_content['data']['process_id'] == os.getpid()
        assert type(response_content['data']['uptime']['uptime']) is int
        assert response_content['data']['uptime']['unit'] == 'seconds'
