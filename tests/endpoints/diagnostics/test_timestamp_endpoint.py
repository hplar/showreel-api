from datetime import datetime

import pytest
from asynctest import patch as async_patch


class TestTimestampEndpoint:
    ENDPOINT = '/diagnostics/timestamp'

    @pytest.mark.freeze_time('1986-07-05 10:10:10')
    async def test_get(self, aiohttp_client, app):
        now = datetime.now()

        client = await aiohttp_client(app)
        response = await client.get(self.ENDPOINT)
        response_content = await response.json()

        assert response.status == 200
        assert response_content['data']['date'] == datetime.strftime(now, '%Y-%m-%d')
        assert response_content['data']['time'] == datetime.strftime(now, '%H:%M:%S')

    async def test_default_format(self, aiohttp_client, app):
        with async_patch('api.endpoints.diagnostics.timestamp.get_timestamp') as mocked_get_timestamp:
            with async_patch('api.endpoints.diagnostics.timestamp.web'):
                client = await aiohttp_client(app)
                _ = await client.get(self.ENDPOINT)

                mocked_get_timestamp.assert_awaited_once_with(
                    date_format='%Y-%m-%d',
                    time_format='%H:%M:%S'
                )
