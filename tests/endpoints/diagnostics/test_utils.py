import os
import sys
from subprocess import check_output
from unittest.mock import patch

import pytest

from api.endpoints.diagnostics.utils import (
    get_linux_distribution,
    get_os_info,
    get_process_id,
    get_python_info,
    get_timestamp,
)


class TestOSInfo:
    async def test_get_linux_info(self):
        uname: list = check_output(['uname', '-a']).decode().split()
        name = uname[0]
        version = uname[2]
        arch = uname[11]

        os_info = {
            'name': name,
            'version': version,
            'architecture': arch,
        }

        try:
            with open('/etc/os-release', 'r') as f:
                os_info['distribution'] = f.readline().strip().split('"')[1]
        except FileNotFoundError:
            os_info['distribution'] = 'unknown'

        assert await get_os_info(platform='linux') == os_info

    async def test_get_linux_distribution_os_release_not_found(self):
        with patch('api.endpoints.diagnostics.utils.open', side_effect=FileNotFoundError):
            distribution = await get_linux_distribution()

        assert distribution == 'unknown'

    async def test_get_macos_info_not_implemented(self):
        with pytest.raises(NotImplementedError):
            await get_os_info(platform='darwin')

    async def test_get_windows_info_not_implemented(self):
        with pytest.raises(NotImplementedError):
            await get_os_info(platform='win32')


async def test_get_python_info():
    impl = sys.implementation
    version = '.'.join([str(n) for n in impl.version[:3]])

    assert await get_python_info() == {
        'implementation': impl.name,
        'version': version,
    }


async def test_get_process_id():
    assert await get_process_id() == os.getpid()


@pytest.mark.parametrize('date_format,time_format,expected_result', [
    ('%Y-%m-%d', '%H:%M:%S', {'date': '1986-07-05', 'time': '10:10:10'}),
    ('%y-%m-%d', '%I:%M:%S %p', {'date': '86-07-05', 'time': '10:10:10 AM'})
])
@pytest.mark.freeze_time('1986-07-05 10:10:10')
async def test_get_timestamp(date_format, time_format, expected_result):
    timestamp = await get_timestamp(date_format=date_format, time_format=time_format)
    assert timestamp == expected_result
