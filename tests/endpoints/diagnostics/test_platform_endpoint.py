import sys
from subprocess import check_output


class TestPlatfromEndpoint:
    ENDPOINT = '/diagnostics/platform'

    async def test_get(self, aiohttp_client, app):
        client = await aiohttp_client(app)

        response = await client.get(self.ENDPOINT)
        response_content = await response.json()

        uname: list = check_output(['uname', '-a']).decode().split()
        name = uname[0]
        version = uname[2]
        arch = uname[11]

        try:
            with open('/etc/os-release', 'r') as f:
                dist = f.readline().strip().split('"')[1]
        except FileNotFoundError:
            pass

        assert response.status == 200
        assert response_content['data']['os']['name'] == name
        assert response_content['data']['os']['version'] == version
        assert response_content['data']['os']['architecture'] == arch
        assert response_content['data']['os']['distribution'] == dist

        assert response_content['data']['python']['version'] == sys.version.split()[0]
