from random import randint

import pytest


class TestInsertionSortEndpoint:
    ENDPOINT = '/sort/insertion'

    @pytest.mark.parametrize('request_json', [
        {'values': [randint(0, 1000) for _ in range(50)]},
    ])
    async def test_insertion_sort_endpoint_with_valid_input(self, request_json, aiohttp_client, app):
        sorted_values = sorted(request_json['values'])
        client = await aiohttp_client(app)

        response = await client.post(self.ENDPOINT, json=request_json)
        assert response.status == 200

        response_json = await response.json()
        assert response_json['data']['values'] == sorted_values

    @pytest.mark.parametrize('request_json', [
        {'values': 12345},
        {'values': 4.1235},
    ])
    async def test_insertion_sort_endpoint_validation_errors(self, request_json, aiohttp_client, app):
        client = await aiohttp_client(app)

        response = await client.post(self.ENDPOINT, json=request_json)
        assert response.status == 422

        response_json = await response.json()
        assert response_json['error']['status'] == 422
        assert response_json['error']['message'] == 'Validation error'

    async def test_get_method_not_allowed(self, aiohttp_client, app):
        client = await aiohttp_client(app)

        response = await client.get(self.ENDPOINT)
        assert response.status == 405

        response_json = await response.json()
        assert response_json['error']['status'] == 405
        assert response_json['error']['message'] == 'Method not allowed'
