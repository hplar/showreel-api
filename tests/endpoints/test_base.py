import json

import pytest
from aiohttp.web import Response
from asynctest import MagicMock

from api.endpoints.base import DefaultContextFactory, EndpointBase, ResponseFactory


class TestEndpointBase:
    ENDPOINT = EndpointBase(MagicMock())

    async def test_get_method_returns_not_allowed_if_not_overridden(self, aiohttp_client, app):
        expected_body = {'error': {'message': 'Method not allowed', 'status': 405}}

        response = await self.ENDPOINT.get()
        assert isinstance(response, Response)
        assert response.status == 405
        assert json.loads(response.body) == expected_body

    async def test_post_method_returns_not_allowed_if_not_overridden(self, aiohttp_client, app):
        expected_body = {'error': {'message': 'Method not allowed', 'status': 405}}

        response = await self.ENDPOINT.post()
        assert isinstance(response, Response)
        assert response.status == 405
        assert json.loads(response.body) == expected_body


class TestDefaultContextFactory:
    FACTORY = DefaultContextFactory

    @pytest.mark.parametrize('status_code,message', [
        (200, 'Ok'),
        (201, 'Created'),
        (404, 'Not found'),
        (405, 'Method not allowed'),
        (422, 'Validation error')
    ])
    async def test_create_default_context(self, status_code, message):
        assert await self.FACTORY.create(status_code=status_code) == {
            'message': message,
            'status': status_code
        }


class TestResponseFactory:
    FACTORY = ResponseFactory

    @pytest.mark.parametrize('status_code,body', [
        (200, {'data': {'message': 'Ok', 'status': 200}}),
        (201, {'data': {'message': 'Created', 'status': 201}}),
        (404, {'error': {'message': 'Not found', 'status': 404}}),
        (405, {'error': {'message': 'Method not allowed', 'status': 405}}),
        (422, {'error': {'message': 'Validation error', 'status': 422}}),
    ])
    async def test_create_with_default_context(self, body, status_code):
        response = await ResponseFactory().create(status_code=status_code)
        assert isinstance(response, Response)
        assert response.status == status_code
        assert json.loads(response.body) == body

    @pytest.mark.parametrize('status_code,field,context', [
        (200, 'data', {'foo': ['bar', 'baz']}),
        (201, 'data', {'foo': {'bar': 'baz'}}),
        (404, 'error', {'foo': 'bar'}),
        (405, 'error', {'foo': 'bar'}),
        (422, 'error', {'foo': [{'bar': 'baz'}]})
    ])
    async def test_create_with_custom_context(self, status_code, field, context):
        response = await ResponseFactory().create(context=context, status_code=status_code)
        assert isinstance(response, Response)
        assert response.status == status_code
        assert json.loads(response.body) == {field: context}
