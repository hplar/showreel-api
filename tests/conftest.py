import aiohttp
import pytest

from api.server import make_app


@pytest.fixture
async def app(loop) -> aiohttp.web_app.Application:
    return make_app()
