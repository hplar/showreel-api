from random import randint

from api.algorithms.sort import Sort


class TestSort:
    VALUES = [randint(0, 1000) for _ in range(0, 50)]
    SORTED_VALUES = sorted(VALUES)

    async def test_bubble_sort(self):
        assert await Sort.bubble(self.VALUES) == self.SORTED_VALUES

    async def test_insertion_sort(self):
        assert await Sort.insertion(self.VALUES) == self.SORTED_VALUES

    async def test_merge_sort(self):
        assert await Sort().merge(self.VALUES) == self.SORTED_VALUES

    async def test_quick_sort(self):
        assert await Sort.quick(self.VALUES) == self.SORTED_VALUES
