from unittest.mock import patch

from api import server


async def test_init():
    with patch.object(server, "main", return_value=42):
        with patch.object(server, "__name__", "__main__"):
            with patch.object(server.sys, 'exit') as mock_exit:
                server.init()
                assert mock_exit.call_args[0][0] == 42


async def test_main():
    with patch('api.server.web.run_app') as mocked_run_app:
        with patch('api.server.make_app') as mocked_make_app:
            server.main()
            assert mocked_make_app.called_once()
            assert mocked_run_app.called_once()
