from typing import Dict, Optional, Type, Union

from aiohttp import web


class DefaultContextFactory:
    @staticmethod
    async def create(status_code: int) -> Dict[str, Union[str, int]]:
        defaults = {
            200: 'Ok',
            201: 'Created',
            404: 'Not found',
            405: 'Method not allowed',
            422: 'Validation error'
        }

        return {
            'message': defaults[status_code],
            'status': status_code
        }


class ResponseFactory:
    def __init__(self, default_context: Type[DefaultContextFactory] = DefaultContextFactory):
        self.default_context = default_context

    async def create(self, context: Optional[dict] = None, status_code: int = 404) -> web.Response:
        if not context:
            context = await self.default_context.create(status_code)

        if status_code < 400:
            context = {'data': context}
        else:
            context = {'error': context}

        return web.json_response(context, status=status_code)


class EndpointBase(web.View):
    def __init__(self, request, response: Type[ResponseFactory] = ResponseFactory):
        super().__init__(request)
        self.response: ResponseFactory = response()

    async def get(self) -> web.Response:
        return await self.response.create(status_code=405)

    async def post(self) -> web.Response:
        return await self.response.create(status_code=405)
