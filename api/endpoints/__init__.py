from api.endpoints.diagnostics.platform import PlatformEndpoint
from api.endpoints.diagnostics.process import ProcessEndpoint
from api.endpoints.diagnostics.timestamp import TimestampEndpoint
from api.endpoints.sort.bubble import BubbleSortEndpoint
from api.endpoints.sort.insertion import InsertionSortEndpoint
from api.endpoints.sort.merge import MergeSortEndpoint
from api.endpoints.sort.quick import QuickSortEndpoint

endpoints = [
    {'url': '/diagnostics/platform', 'handler':  PlatformEndpoint},
    {'url': '/diagnostics/process', 'handler':  ProcessEndpoint},
    {'url': '/diagnostics/timestamp', 'handler':  TimestampEndpoint},
    {'url': '/sort/bubble', 'handler':  BubbleSortEndpoint},
    {'url': '/sort/insertion', 'handler':  InsertionSortEndpoint},
    {'url': '/sort/merge', 'handler':  MergeSortEndpoint},
    {'url': '/sort/quick', 'handler':  QuickSortEndpoint}
]
