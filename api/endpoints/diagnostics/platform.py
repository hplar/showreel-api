from aiohttp import web
from aiohttp_apispec import docs, response_schema

from api.endpoints.base import EndpointBase
from api.endpoints.diagnostics.schemas.platform import PlatformResponseSchema
from api.endpoints.diagnostics.utils import get_os_info, get_python_info


class PlatformEndpoint(EndpointBase):
    @docs(
        tags=['Diagnostics'],
        summary='Get information about the platform the application is running on',
        description='Get information about the platform the application is running on',
    )
    @response_schema(PlatformResponseSchema())
    async def get(self) -> web.Response:
        os_info: dict = await get_os_info()
        python_info: dict = await get_python_info()

        context: dict = {
            'os': os_info,
            'python': python_info
        }

        return await self.response.create(context=context, status_code=200)
