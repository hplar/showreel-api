from aiohttp import web
from aiohttp_apispec import docs, response_schema

from api.endpoints.base import EndpointBase
from api.endpoints.diagnostics.schemas.process import ProcessResponseSchema
from api.endpoints.diagnostics.utils import get_process_id, get_uptime


class ProcessEndpoint(EndpointBase):
    @docs(
        tags=['Diagnostics'],
        summary='Get information about the application process',
    )
    @response_schema(ProcessResponseSchema())
    async def get(self) -> web.Response:
        uptime: dict = await get_uptime()
        process_id: int = await get_process_id()

        context: dict = {
            'uptime': uptime,
            'process_id': process_id,
        }

        return await self.response.create(context=context, status_code=200)
