import os
import sys
import time
from datetime import datetime
from subprocess import check_output

import psutil


async def get_linux_distribution() -> str:
    distribution = 'unknown'

    try:
        with open('/etc/os-release', 'r') as f:
            distribution = f.readline().strip().split('"')[1]
    except FileNotFoundError:
        pass

    return distribution


async def _get_linux_info() -> dict:
    uname: list = check_output(['uname', '-a']).decode().split()
    distribution: str = await get_linux_distribution()

    info: dict = {
        'name': uname[0],
        'version': uname[2],
        'architecture': uname[11],
        'distribution': distribution
    }

    return info


async def get_os_info(platform: str = 'linux', info: dict = None) -> dict:
    if not info:
        info = {}

    if platform == 'linux':
        info = await _get_linux_info()
    elif platform == 'darwin':
        raise NotImplementedError
    elif platform == 'win32':
        raise NotImplementedError

    return info


async def get_process_id() -> int:
    return os.getpid()


async def get_python_info() -> dict:
    impl = sys.implementation

    version: str = '.'.join([str(n) for n in impl.version[:3]])

    python_info: dict = {
        'implementation': impl.name,
        'version': version,
    }

    return python_info


async def get_timestamp(date_format: str = '%Y-%m-%d', time_format: str = '%H:%M:%S') -> dict:
    now: datetime = datetime.now()
    current_date: str = datetime.strftime(now, date_format)
    current_time: str = datetime.strftime(now, time_format)

    timestamp: dict = {
        'date': current_date,
        'time': current_time,
    }

    return timestamp


async def get_uptime() -> dict:
    epoch_time: int = int(time.time())

    current_process: psutil.Process = psutil.Process(os.getpid())
    process_time: int = int(current_process.create_time())

    uptime: int = epoch_time - process_time
    unit: str = 'seconds'

    uptime_dict: dict = {
        'uptime': uptime,
        'unit': unit,
    }

    return uptime_dict
