from aiohttp import web
from aiohttp_apispec import docs, response_schema

from api.endpoints.base import EndpointBase
from api.endpoints.diagnostics.schemas.timestamp import TimestampResponseSchema
from api.endpoints.diagnostics.utils import get_timestamp


class TimestampEndpoint(EndpointBase):
    @docs(
        tags=['Diagnostics'],
        summary='Get the current date and time',
    )
    @response_schema(TimestampResponseSchema())
    async def get(self) -> web.Response:
        timestamp: dict = await get_timestamp(date_format='%Y-%m-%d', time_format='%H:%M:%S')
        return await self.response.create(context=timestamp, status_code=200)
