from marshmallow import Schema, fields


class TimestampResponseSchema(Schema):
    date: fields.String = fields.Str()
    time: fields.String = fields.Str()
