from marshmallow import Schema, fields


class UptimeSchema(Schema):
    count: fields.Integer = fields.Int()
    unit: fields.String = fields.Str()


class ProcessResponseSchema(Schema):
    uptime: fields.List = fields.List(fields.Nested(UptimeSchema))
    process_id: fields.Integer = fields.Int()
