from marshmallow import Schema, fields


class OSSchema(Schema):
    name: fields.String = fields.Str()
    distribution: fields.String = fields.Str()
    version: fields.String = fields.Str()
    architecture: fields.String = fields.Str()


class PythonSchema(Schema):
    implementation: fields.String = fields.Str()
    version: fields.String = fields.Str()


class PlatformResponseSchema(Schema):
    os: fields.List = fields.List(fields.Nested(OSSchema))
    python: fields.List = fields.List(fields.Nested(PythonSchema))
