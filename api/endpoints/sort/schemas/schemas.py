from marshmallow import Schema, fields


class SortRequestSchema(Schema):
    values: fields.List = fields.List(fields.Int(), required=True)


class SortResponseSchema(Schema):
    values: fields.List = fields.List(fields.Int())
