from typing import Dict

from aiohttp import web
from aiohttp_apispec import docs, request_schema

from api.algorithms.sort import Sort
from api.endpoints.base import EndpointBase
from api.endpoints.sort.schemas.schemas import SortRequestSchema, SortResponseSchema


class MergeSortEndpoint(EndpointBase):
    @docs(
        tags=['Sorting'],
        summary='Sort a list of integers with the merge sort algorithm',
        responses={
            200: {'schema': SortResponseSchema},
            405: {'description': 'Method not allowed'},
            422: {'description': 'JSON validation error'}
        }
    )
    @request_schema(SortRequestSchema)
    async def post(self) -> web.Response:
        request_json: Dict = await self.request.json()

        if not isinstance(request_json['values'], list):
            return await self.response.create(status_code=422)

        context: Dict = {
            'values': await Sort().merge(request_json['values'])
        }

        return await self.response.create(context=context, status_code=200)
