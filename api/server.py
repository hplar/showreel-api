#!/usr/bin/env python3

import sys
from typing import List

from aiohttp import web, web_app
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from api.endpoints import endpoints


def make_app() -> web.Application:
    # configure middlewares
    middlewares: List = [validation_middleware]

    # create app
    app: web_app.Application = web.Application(middlewares=middlewares)

    # configure aiohttp_apispec
    setup_aiohttp_apispec(
        app=app,
        title='Showreel API',
        version='v1',
        url='/docs/swagger.json',
        swagger_path='/docs',
    )

    for endpoint in endpoints:
        app.router.add_view(endpoint['url'], endpoint['handler'])

    return app


def main():
    app: web.Application = make_app()
    web.run_app(app)


def init():
    if __name__ == '__main__':
        sys.exit(main())


init()
