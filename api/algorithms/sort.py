import asyncio
from typing import List


class Sort:
    def __init__(self):
        ...

    @staticmethod
    async def bubble(values: List[int]) -> List[int]:
        last_unsorted_index: int = len(values) - 1

        while last_unsorted_index > 0:
            for i in range(last_unsorted_index):
                l_val: int = values[i]
                r_val: int = values[i + 1]

                if l_val > r_val:
                    values[i], values[i + 1] = values[i + 1], values[i]

            last_unsorted_index -= 1
            await asyncio.sleep(0.01)

        return values

    @staticmethod
    async def insertion(values: List[int]) -> List[int]:
        for i in range(1, len(values)):
            current_value: int = values[i]

            while i > 0 and values[i - 1] > current_value:
                values[i] = values[i - 1]
                i = i - 1

            values[i] = current_value

            await asyncio.sleep(0.001)

        return values

    async def merge(self, values: List[int]) -> List[int]:
        if len(values) > 1:
            left_subset: list = values[:len(values) // 2]
            right_subset: list = values[len(values) // 2:]

            await asyncio.wait([
                self.merge(left_subset),
                self.merge(right_subset)
            ])

            i: int = 0
            j: int = 0
            k: int = 0

            while i < len(left_subset) and j < len(right_subset):
                if left_subset[i] < right_subset[j]:
                    values[k] = left_subset[i]
                    i += 1
                else:
                    values[k] = right_subset[j]
                    j += 1
                k += 1

            while i < len(left_subset):
                values[k] = left_subset[i]
                i += 1
                k += 1

            while j < len(right_subset):
                values[k] = right_subset[j]
                j += 1
                k += 1

            await asyncio.sleep(0.01)

        return values

    @staticmethod
    async def quick(values: List[int]) -> List[int]:
        last_unsorted_index: int = len(values) - 1

        while last_unsorted_index > 0:
            for i in range(last_unsorted_index):
                l_val: int = values[i]
                r_val: int = values[i + 1]

                if l_val > r_val:
                    values[i], values[i + 1] = values[i + 1], values[i]

            last_unsorted_index -= 1
            await asyncio.sleep(0.01)

        return values
