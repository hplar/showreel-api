# Showreel API

This API runs on Python3 and AIOHTTP and is solely meant for showcasing purposes.
It is not meant to be run on any kind of production or outward facing environment
and has endpoints that disclose information about it's process and host system
that should be considered insecure.

## Requirements

* Python 3.8

## Usage

### Installing the virtual environment

Make target

```bash
make venv
```

Manually

```ffdd
python3 -m venv
./venv/bin/python3 -m pip install -r ./req/base.txt
```

### Running the server

Make target

```bash
make serve
```

Manually

```bash
source venv/bin/activate
PYTHONPATH=. ./src/server.py
```

## Development

### Installing the development virtual environment

Make target

```bash
make venv/dev
```

Manually

```bashV
python3 -m venv
./venv/bin/python3 -m pip install -r ./req/base.txt -r ./req/dev.txt -r ./req/tox.txt
```

### Running the development server (autoreload)

Make target

```bash
make serve/dev
```

Manually

```bash
source venv/bin/activate
adev runserver ./src/server.py
```

## Testing

### Full test suite

Make target

```bash
make test
```

### Unit tests

**To run unittests the development virtual environment must be installed and activated**

Make target

```bash
make test/unit
```

Manualy

```bash
tox -e py38
```

## Code quality

### Lint

Make target

```bash
make test/lint
```

Manually

```bash
mypy ./src
tox -e lint,isort
```

## Swagger

The swagger docs can be viewed by browsing to

```bash
http://localhost:8080/docs
```

The dev server swagger docs can be viewed by browsing to

```bash
http://localhost:8000/docs
```
